package com.fixedIP.fixedIP.repository;

import com.fixedIP.fixedIP.model.EmployeeVO;
import com.fixedIP.fixedIP.model.IpVO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@org.springframework.stereotype.Repository
@Mapper
public interface MainRepository {

    public List<IpVO> getIpList();

    public List<EmployeeVO> getEmployee(int id);

    public void updateIP(int ip, int id);

    public void delete(int ip);
}
