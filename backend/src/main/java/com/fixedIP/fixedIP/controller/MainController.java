package com.fixedIP.fixedIP.controller;

import com.fixedIP.fixedIP.model.*;
import com.fixedIP.fixedIP.service.MainService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping
public class MainController {
    @Autowired
    private final MainService service;

    @GetMapping("ip_list")
    public ResponseEntity<?> getList() {
        List<IpVO> list = service.getIpList();
        ResponseVO<IpVO> response = ResponseVO.<IpVO>builder().data(list).build();
        return ResponseEntity.ok().body(response);
    }

    @PostMapping("employee")
    public ResponseEntity<?> getEmployee(@RequestBody IntVO id) {
        try {
            List<EmployeeVO> list = service.getEmployee(id.getNum());
            ResponseVO<EmployeeVO> response = ResponseVO.<EmployeeVO>builder().data(list).build();
            return ResponseEntity.ok().body(response);
        } catch(Exception e) {
            String error = e.getMessage();
            ResponseVO<EmployeeVO> response = ResponseVO.<EmployeeVO>builder().error(error).build();
            return ResponseEntity.badRequest().body(response);
        }
    }

    @PutMapping("ip")
    public ResponseEntity<?> registerIP(@RequestBody IpIntVO reg) {
        List<IpVO> list = service.updateIP(reg.getIp(), reg.getId());
        ResponseVO<IpVO> response = ResponseVO.<IpVO>builder().data(list).build();
        return ResponseEntity.ok().body(response);
    }

    @DeleteMapping("ip")
    public ResponseEntity<?> removeIP(@RequestBody IntVO ip) {
        service.delete(ip.getNum());
        List<String> list = new ArrayList<>();
        list.add("delete completed");
        ResponseVO<String> response = ResponseVO.<String>builder().data(list).build();
        return ResponseEntity.ok().body(response);
    }
}
