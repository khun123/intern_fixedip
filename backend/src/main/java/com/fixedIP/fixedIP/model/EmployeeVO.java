package com.fixedIP.fixedIP.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class EmployeeVO {


    public int id;

    public String name;

    public String team;

    public String nickname;

    public String workspace;
}
