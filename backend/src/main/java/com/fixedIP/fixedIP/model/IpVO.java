package com.fixedIP.fixedIP.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.validation.constraints.NotNull;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class IpVO {

    @NotNull
    public int ip;

    public int id;

    public boolean allocated;

    public String name;

    public String team;

    public String nickname;

    public String workspace;

}
