package com.fixedIP.fixedIP.service;

import com.fixedIP.fixedIP.model.EmployeeVO;
import com.fixedIP.fixedIP.model.IpVO;
import com.fixedIP.fixedIP.repository.MainRepository;
import lombok.RequiredArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@org.springframework.stereotype.Service
@RequiredArgsConstructor
public class MainService {

    private final MainRepository repo;

    public List<IpVO> getIpList() {
        return repo.getIpList();
    }

    public List<EmployeeVO> getEmployee(int id) {

        var temp = repo.getEmployee(id);
        if(temp.isEmpty()) {
            throw new RuntimeException("존재하지 않는 사원입니다.");
        }
        else return repo.getEmployee(id);
    }

    public List<IpVO> updateIP(int ip, int id) {
        repo.updateIP(ip, id);
        EmployeeVO emp = repo.getEmployee(id).get(0);
        List<IpVO> result = new ArrayList<>();
        IpVO temp = IpVO.builder()
                 .ip(ip)
                .id(id)
                .name(emp.getName())
                .nickname(emp.getNickname())
                .team(emp.getTeam())
                .workspace(emp.getWorkspace())
                .allocated(true)
                .build();
        result.add(temp);
        return result;

    }

    public void delete(int ip) {
        repo.delete(ip);
    }
}
