USE fixedIP;
DROP PROCEDURE IF EXISTS create_ip;
DELIMITER $$
CREATE PROCEDURE create_ip()
BEGIN
    DECLARE i INT DEFAULT 51;
    WHILE (i <= 100) DO
        INSERT INTO IP VALUES(i, NULL, FALSE);
        SET i = i + 1;
    END WHILE;
END $$
DELIMITER ;

CALL create_ip();


INSERT INTO Employee VALUES(84968, "김경훈", "CNN", "Michael", "본사 5층");
INSERT INTO Employee VALUES(11111, "김철수", "모던어플리케이션", "Thomas", "13동 5층");
INSERT INTO Employee VALUES(22222, "김유나", "CNN", "GOD", "선릉역");
INSERT INTO Employee VALUES(85000, "이선수", "모던어플리케이션", "John", "14동 5층");
INSERT INTO Employee VALUES(85001, "이철수", "테스트자동화", "Cheol", "14동 5층");
INSERT INTO Employee VALUES(85002, "김지나", "CNN", "Gina", "10동 5층");
INSERT INTO Employee VALUES(85003, "김태호", "모던어플리케이션", "Infinite", "여의도");
INSERT INTO Employee VALUES(85004, "강형구", "애자일이행팀", "Wind", "역삼역");
INSERT INTO Employee VALUES(85005, "강지윤", "애자일이행팀", "GOF", "서울역");
INSERT INTO Employee VALUES(85006, "김하나", "Intelligent Product팀", "HANA", "서울역");
INSERT INTO Employee VALUES(85007, "장진우", "CNN", "Jinwoo", "미주법인");
INSERT INTO Employee VALUES(85008, "김진우", "Intelligent Product팀", "speero", "미주법인");
INSERT INTO Employee VALUES(85009, "김영우", "테스트자동화", "Young", "종로");

