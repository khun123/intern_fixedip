USE fixedIP;
CREATE TABLE Employee(
	id INT(11) PRIMARY KEY NOT NULL,
    name VARCHAR(20),
    team VARCHAR(32),
    nickname VARCHAR(20),
    workspace VARCHAR(30)
);

CREATE TABLE IP (
	ip INT(11) PRIMARY KEY NOT null,
    id INT(11),
    allocated BOOLEAN,
    FOREIGN KEY (id) REFERENCES Employee(id)
);