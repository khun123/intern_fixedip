import React from "react";
import TableItem from "./TableItem";

function Table({ columns, data, toggle }) {

  return (
    <table border = "1" align="center">
      <thead>
        <tr>
          {columns.map((column) => (
            <th key={column} className={column}>{column}</th>
          ))}
        </tr>
      </thead>
      <tbody>
        {data.map((item) => <TableItem item = {item} key = {item.ip + item.id} toggle = {toggle} />)}
      </tbody>
    </table>
  );
}

export default Table;