import React, {useState} from "react";
import { call } from "./service/ApiService";

class Id {
    constructor(id) {
        this.num = id;
    }
}

class Ipid {
    constructor(ip, id) {
        this.ip = ip;
        this.id = id;
    }
}

const dummy = {"name": "", "team": "", "nickname": "", "workspace": ""};

function InputField (props) {
    const {data, add} = props;
    const [ip, setIp] = useState("");
    const [empId, setEmpId] = useState("");
    const [empInfo, setEmpInfo] = useState(dummy);


    const enterKeyEventHandler = (event) => {
        if(event.key === "Enter") {
            call("/employee", "POST", new Id(empId))
                .then((response) => { setEmpInfo(response.data[0]); })
                .catch((response) => { alert(response.error); setEmpInfo({...dummy});})

        }
    }

    const allocateKeyEventHandler = () => {
        if(ip === "") {
            alert("IP를 입력하십시오.");
            return;
        }
        if(ip < 51 || ip > 100) {
            alert("IP는 51에서 100 범위 안에 있어야합니다.");
            return;
        }
        if(data.filter((item) => item.ip == ip)[0].allocated) {
            alert("이미 할당된 IP입니다.");
            return;
        }
        if(empInfo.name == "" || empId == "") {
            alert("사원번호를 입력하고 엔터를 누르십시오.");
            return;
        }
        if(data.filter((item) => item.id == empId).length === 1) {
            alert("이미 IP를 할당받은 사원입니다.");
            return;
        }
        call("/ip", "PUT", new Ipid(ip, empId))
            .then((response) => { 
                add(data.map(item => item.ip == ip ? {...item, allocated: true, name: empInfo.name, id: empId, team: empInfo.team, nickname: empInfo.nickname, workspace: empInfo.workspace}: item)); 
                setIp("");
                setEmpId("");
                setEmpInfo({...dummy});
        })
            .catch((response) => { 
                alert(response.error); 
                setEmpInfo({...dummy});}
            )
    }

    const ipEditEventHandler = (event) => {
        setIp(event.target.value);
    }

    const idEditEventHandler = (event) => {
        setEmpId(event.target.value);
    }

    return (
        <div className = "allocate">
            <div style = {{fontSize: "41.5px"}}> &nbsp;</div>
            <fieldset className="inputField">
              <legend>IP 할당하기</legend>
              <div style = {{margin: "auto", textAlign: "center"}}>
                <div style = {{display: "inline-block", marginBottom: "16px"}}>
                  <label>IP: 10.81.131.<input  className="inputForm" type="text"  value = {ip} border = "none" onChange={ipEditEventHandler}/></label>
                  <label  onKeyPress={enterKeyEventHandler}>사원번호: <input  className="inputForm" type="text" value = {empId} onChange={idEditEventHandler}/></label>
                </div>
                {empInfo.name === "" ? (<div style = {{fontSize: "41.5px"}}> &nbsp;</div>) : (<div style = {{display: "inline-block", marginBottom: "16px"}}>
                    <label  >사원명: <input className="inputForm" type="text" value = {empInfo.name} disabled/></label>
                  <label>팀: <input className="inputForm" type="text" value = {empInfo.team}  disabled/></label>
                <div style = {{display: "inline-block", marginBottom: "16px"}}></div>
                  <label>닉네임: <input className="inputForm" type="text" value = {empInfo.nickname} disabled/></label>
                  <label>근무지: <input className="inputForm" type="text" value = {empInfo.workspace} disabled/></label>
                </div>)}
                <div>
                    <input type="submit" onClick = {allocateKeyEventHandler} value = "할당하기" className="inputButton"/>
                </div>
              </div>
            </fieldset>
        </div>
    );
}

export default InputField;