import React, {useState, useEffect} from "react";
import Table from "./Table";
import InputField from "./InputField";
import { call } from "./service/ApiService";

const columns = ["IP", "사용여부", "사원번호", "사원명", "팀", "닉네임", "근무지", "작업"];


function Mainpage() {

  const [data, setData] = useState([]);
  const [value, setValue] = useState("all");
  const [toggle, setToggle] = useState(true);

   useEffect(() => {
        call("/ip_list", "GET", null).then((response) => { setData(response.data) });
   }, [toggle]);



    const select = (event) => {
      setValue(event.target.value);
    }

    const handleData = (data, val) => {
      if(val === "all") return data;
      else if (value === "allocated") return (data.filter((item) => item.allocated === true));
      else return (data.filter((item) => item.allocated === false));
    }


  return (
    <div className = "container">
        <InputField data = {data} add = {setData}/>
        <select className="filter" onChange={select} >  
            <option value="all" selected >전체 보기</option>  
            <option value="allocated">사용중</option>  
            <option value="unallocated">미사용</option>  
        </select>  
        <Table columns={columns} data={handleData(data, value)} toggle = {setToggle} />
    </div>
  );
}

export default Mainpage;