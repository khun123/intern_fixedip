import { API_BASE_URL } from "../app-config";

export function call(api, method, request) {
    let options = {
        headers: new Headers({
            "Content-Type": "application/json",
        }),
        mode: 'cors',
        url: API_BASE_URL + api,
        method: method,
    };
    if(request) {
        options.body = JSON.stringify(request);
    }
    return fetch(options.url, options).then((response) =>
    response.json().then((json) => {
        if(!response.ok) {
            return Promise.reject(json);
        }
        return json;
        })
    )
    .catch((error) => {
        console.log(error.status);
        if(error.status === 403) {
            alert("존재하지 않는 사원입니다.");
        }

        return Promise.reject(error);
    });
}