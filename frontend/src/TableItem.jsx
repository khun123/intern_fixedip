import React, {useState} from "react";
import { call } from "./service/ApiService";
const dummy = {allocated: false, "name": "", "team": "", "nickname": "", "workspace": ""};

class Ip {
    constructor(ip) {
        this.num = ip;
    }
}

function TableItem(props) {
    const {item, toggle} = props;
    const [row, setRow] = useState(item);
    const unallocate = () => {
        call("/ip", "DELETE", new Ip(item.ip))
        .then((response) => { setRow({...dummy, ip: item.ip}); toggle((prev) => (!prev)); })
        .catch((response) => { 
            alert(response.error); 
        }
        )
    }

    const usedButton = <input type="button" value = "사용중" className="usedButton" />;
    const unUseButton = <input type="button" value = "미사용" className="unUsedButton" />;
    const unallocateButton = <input type="button" value = "해제" className="unallocateButton" onClick={unallocate}/>;

    return (
        <tr >
            <td>{row.ip}</td>
            <td>{row.allocated ? usedButton : unUseButton}</td>
            <td>{row.allocated ? row.id: ""}</td>
            <td>{row.name}</td>
            <td>{row.team}</td>
            <td>{row.nickname}</td>
            <td>{row.workspace}</td>
            <td>{row.allocated ? unallocateButton : ""}</td>
        </tr>
    );
}

export default TableItem;